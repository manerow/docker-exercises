## 2. [1 pto] Indica la diferencia entre el uso de la instrucción ADD y COPY (Dockerfile).
COPY y ADD són instrucciones de Dockerfile que cumplen propósitos similares. Nos permiten copiar ficheros desde una localización específica hacia dentro de nuestra imagen Docker.
Sus diferencias consisten en lo siguiente: 
 - Por un lado <ins>COPY</ins> permite copiar un fichero local de la máquina host dentro de la imagen Docker. Especificando el origen de esta en la máquina host y el destino en la imagen docker. <br><br>
 - <ins>ADD</ins>, por otro lado, nos proporciona las mismas funcionalidades que COPY y además nos permite dos funcionalidades más:
    1. Establecer una URL como origen del fichero a copiar dentro de la imagen Docker.
    2. Extraer archivos comprimidos en el origen directamente a dentro de la imagen Docker.



