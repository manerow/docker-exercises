## 5. [3 ptos] Realizar POC de implementación de una Elastic Stack con ElasticSearch y Kibana.
#### 1. Desarrollo del fichero de configuración de docker-compose.
Para realizar esta POC empiezo con el desarrollo del fichero docker-compose.yml siguiente:
```
version: '3.6'

services:
  elasticsearch:
  # Utilizar la imagen de elasticsearch v7.9.3.
    image: elasticsearch:7.9.3
  # Asignar un nombre al contenedor.
    container_name: elasticsearch
  # Definir variables de entorno.
    environment:
      - discovery.type=single-node
  # Emplazar el contenedor a la red de elastic.
    networks: 
      - elastic
  # Mapear puertos
    ports: 
      - 9200:9200
      - 9300:9300
      
  kibana:
  # Utilizar la imagen kibana v7.9.3.
    image: kibana:7.9.3
  # Asignar un nombre al contenedor.
    container_name: kibana
  # Definir variables de entorno.
    environment: 
      - ELASTICSEARCH_HOST=elasticsearch
      - ELASTICSEARCH_PORT=9200
  # Emplazar el contenedor a la red de elastic
    networks:
      - elastic
  # Mapear puertos.
    ports:
      - 5601:5601
  # El contenedor Kibana depe esperar a la disponibilidad del servicio elasticsearch
    depends_on: 
      - elasticsearch
    
# Definir la red elastic (bridge)
networks:
  elastic:
    driver: bridge

```
#### 2. Levantar los servicios
Posteriormente ejecuto: 
```
$ docker-compose up -d
```
para levantar los servicios y network que he definido.

#### 3. Visualización de la web.
Seguidamente me dirijo a http://localhost:5601 <br>
![Página principal](.bin/img/ex-5_01.png)

A continuación selecciono la opción de "Try our sample data" <br>
![Opcion add data](.bin/img/ex-5_02.png)

Pulso en "Add data" en una de las opciones <br>
![Pulsar en add data](.bin/img/ex-5_03.png)

Me dirijo a view data -> Dashboard <br>
![Pulsar en add data](.bin/img/ex-5_04.png)

