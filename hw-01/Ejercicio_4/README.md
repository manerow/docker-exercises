## 4. [2,5 ptos] Crea una imagen docker a partir de un Dockerfile. Esta aplicación expondrá un servicio en el puerto 8080 y se deberá hacer uso de la instrucción HEALTHCHECK para comprobar si la aplicación está ofreciendo el servicio o por si el contrario existe un problema.
- La prueba se realizará cada 45 segundos.
- Por cada prueba realizada, se esperará que la aplicación responda en menos de 5 segundos. Si tras 5 segundos no se obtiene respuesta, se considera que la prueba habrá fallado.
- Ajustar el tiempo de espera de la primera prueba.
- El número de reintentos será 2. Si fallan dos pruebas consecutivas, el contenedor deberá cambiar al estado “unhealthy”.

### Desarrollo del Dockerfile.
Para realizar el siguiente ejercicio he creado una pequeña aplicación en Golang que me permite exponer una API REST en el puerto 8080 de mi localhost con un solo endpoint de tipo “GET” llamado “/health”. El endpoint devuelve el parámetro “isAlive” = true. <br>

Una vez creada la aplicación he desarrollado el correspondiente Dockerfile para poder ejecutarla. El Dockerfile se corresponde al siguiente: <br>
```
#-------------------
# STAGE 1
#-------------------
ARG GO_VERSION=1.17.1
FROM golang:${GO_VERSION}-alpine as builder

# Install git and ca-certificates
RUN apk update && apk add --no-cache git
# Create group and user
RUN addgroup -S gogroup && adduser -S -u 10000 -g gogroup gouser

# Change workdir
WORKDIR /src
# Copy the project inside the container
COPY ./ ./
# Download dependencies
RUN go mod download 
# Build project.
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o app ./cmd

#-------------------
# STAGE 2
#-------------------
FROM alpine:3.14

# Copy users
COPY --from=builder /etc/passwd /etc/passwd
# Copy executable
COPY --from=builder /src/app /app

RUN apk add --no-cache curl

# Change default user
USER gouser

# Expose container port 8080
EXPOSE 8080

# Do the Healthcheck
HEALTHCHECK --start-period=6s --interval=45s --timeout=5s --retries=2 CMD curl -fs http://localhost:8080/health || exit 1

# Set the entrypoint on the app executable file
ENTRYPOINT ["/app"]

```

Se podría reducir aún más el tamaño de la imagen resultante usando la imagen “scratch” en la segunda stage del Dockerfile. El motivo de no haberlo hecho de este modo es el hecho que la imagen scratch no disponga del comando curl, ni de una shell. Por este hecho, me ha sido imposible conseguir ejecutar el healthcheck en la imagen scratch y he optado hacerlo con la imagen alpine.
<br><br>
Respecto a la configuración y ejecución del Healthcheck, se ha seguido los siguientes pasos: <br>
#### 1. Determinar el tiempo en el que han de empezar a iniciarse las pruebas. 
He supuesto que el tiempo que tarda en iniciarse puede calcularse mediante la siguiente fórmula: 
```
Tiempo_inicio = timestamp_CreatedAt - timestamp_StartedAt
```
Para comprobar el CreatedAt he ejecutado el siguiente comando: 
```
docker inspect --format='{{.Created}}' <Container_id>
```
Para comprobar el StartedAt he ejecutado el siguiente comando: 
```
docker inspect --format='{{.State.StartedAt}}' <Container_id>
```
El Tiempo_inicio resultante ha sido de aproximadamente de 30 milisegundos. Como este tiempo es variable entre distintas ejecuciones tomaré un tiempo de inicio de 1 segundo.<br>
Tal y como indica el enunciado, debemos sumarle unos 5 segundos al tiempo de inicio para calcular el tiempo en que empezaran a contar las pruebas Healthcheck. Por lo tanto, el tiempo de inicio de las pruebas será de 6 segundos. <br>
El flag que nos permite asignar este valor es:
``` 
--start-period=6s 
```
#### 2. Asignar el intervalo entre pruebas.
Este requerimiento se consigue mediante el flag 
```
--interval=45s
```
#### 3. Asignar el tiempo de espera a despues de ejecutar una prueba a 5 segundos para considerar ésta como inválida.
Este requerimiento se consigue mediante el flag 
```
--timeout=5s
```
#### 4.Asignar el numero de reintentos a un máximo de 2.
Este requerimiento se consigue mediante el flag 
```
--retries=2
```
#### 5. Indicar al healthcheck la instrucción que le permitirá determinar el estado de la aplicación. 
```
CMD curl --fs http://localhost:8080/health || exit 1 
```

### Comprobación del resultado.
Finalmente construimos y ejecutamos la imagen y nos disponemos a comprobar los logs del contenedor. Para ello inspeccionamos la información del contenedor mediante <br>
```
$ docker inspect <Container_id>
```
Como podemos comprobar se han ejecutado un total de 2 pruebas. Como las 2 pruebas ejecutadas dentro del tiempo de comprobaciones han sido favorables, el contenedor se ha marcado con el estado Healthy.<br>
![Logs del healthcheck](.bin/img/ex-4_01.png)
<br>Comprobamos el resultado: <br>
![Logs del healthcheck](.bin/img/ex-4_02.png)



