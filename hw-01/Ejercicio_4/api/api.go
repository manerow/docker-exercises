package api

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

// App export
type Api struct {
	Router *mux.Router
}

type Health struct {
	IsAlive bool `json:"isAlive"`
}

func isAliveHandler(w http.ResponseWriter, r *http.Request) {
	data := Health{IsAlive: true}
	respondWithJSON(w, http.StatusOK, data)
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(payload)
}

func (api *Api) InitialiseRoutes() {
	api.Router = mux.NewRouter()
	api.Router.HandleFunc("/health", isAliveHandler).Methods("GET")
}

func (api *Api) Run(port string) {
	fmt.Println("App was initialized on port", port)
	http.ListenAndServe(":"+port, api.Router)
}
